#! /usr/bin/python
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium import webdriver
import urllib
from bs4 import BeautifulSoup
import urlparse
import mechanize
import re

URL1 = "https://www.youwin.com/sports-football/england-premier-league/1-102-386195.html" # user: AIRS023 password: T4K3B7F8O2
URL2 = "http://www.pinnaclesports.com/en/odds/match/soccer/england/england-premier-league" # user: MT824253  password: @WELCOME1 https://www.pinnaclesports.com/en/login

#br = mechanize.Browser()
#br.set_proxies({"https": "128.199.190.192:3128"})
#br.open(url)

#html = br.open(url)
#html = urllib.urlopen(url) 


wd2 = webdriver.Firefox()
wd2.get(URL2)


# Wait for the dynamically loaded elements to show up
WebDriverWait(wd2, 5).until(
    EC.visibility_of_element_located((By.CLASS_NAME, "ng-scope"))) 

html_page2 = wd2.page_source
wd2.quit()

# Now you can use html_page as you like

soup2 = BeautifulSoup(html_page2, "html.parser")

#soup = BeautifulSoup(html , "html.parser")
# use this (gets all <td> elements)
#cols = soup.findAll('td')
# or this (gets only <td> elements with class='h3')
#cols = soup.findAll('a', attrs={"class" : 'ko-option-template option'})
#print cols[0].renderContents() 

dataSite2 = {}
secSiteEvent = [] #Array to store the event item  {first site}
secSiteTime = [] #Array to store the event item data time {first site}
secSiteOdds = [] #Array to store the event item odds {first site}

r1 = re.compile('[A-z]') #search for the letters
r2 = re.compile('[0-9]') #search for the numbers  
# SITE 1 SCRAPING BEGIN ################################################
for eventName in soup2.find_all('span', attrs={"class" : 'ng-binding ng-scope'}):
     if (eventName.get_text()).find('Draw')==-1:
         if (filter(r1.match, eventName.get_text())):
             secSiteEvent.append(eventName.get_text())
             #print eventName.get_text()
         if (filter(r2.match, eventName.get_text())):
             secSiteOdds.append(eventName.get_text())
             #print eventName.get_text()    

# SITE 1 SCRAPING END ##################################################    

# Normalize the data from site 2 #

jumperFT1 = 0
jumperFT2 = 1

junperT1 = 0 
junperDraw = 2
junperT2 = 1 
for el in range(len(secSiteEvent)):
    if (el < (len(secSiteOdds)/3)):
            dataSite2[el] = {
               'teams': secSiteEvent[jumperFT1] + " v " + secSiteEvent[jumperFT2],
               'firstTeamWin': secSiteOdds[junperT1],
               'draw': secSiteOdds[junperDraw],
               'secondTeamWin': secSiteOdds[junperT2]
            }
            jumperFT1 += 2
            jumperFT2 += 2
            junperT1 += 3 
            junperDraw += 3
            junperT2 += 3

print dataSite2
#print dataSite2[0]['teams']