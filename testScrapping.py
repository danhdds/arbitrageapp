#! /usr/bin/python
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium import webdriver
import urllib
from bs4 import BeautifulSoup
import urlparse
import mechanize
import re
from decimal import Decimal
import cookielib 
import urllib2 
import mechanize 

URL1 = "https://www.youwin.com/sports-football/england-premier-league/1-102-386195.html" # user: AIRS023 password: T4K3B7F8O2
URL2 = "http://www.pinnaclesports.com/en/odds/match/soccer/england/england-premier-league" # user: MT824253  password: @WELCOME1 https://www.pinnaclesports.com/en/login

#br = mechanize.Browser()
#br.set_proxies({"https": "128.199.190.192:3128"})
#br.open(url)

#html = br.open(url)
#html = urllib.urlopen(url) 

# Start the WebDriver and load the page
wd1 = webdriver.Firefox()
wd1.get(URL1)

wd2 = webdriver.Firefox()
wd2.get(URL2)

# Wait for the dynamically loaded elements to show up
WebDriverWait(wd1, 6).until(
    EC.visibility_of_element_located((By.CLASS_NAME, "couponEvents")))

# And grab the page HTML source
html_page1 = wd1.page_source
wd1.quit()

# Wait for the dynamically loaded elements to show up
WebDriverWait(wd2, 6).until(
    EC.visibility_of_element_located((By.CLASS_NAME, "ng-scope"))) 

html_page2 = wd2.page_source
wd2.quit()

# Now you can use html_page as you like
soup1 = BeautifulSoup(html_page1, "html.parser")
soup2 = BeautifulSoup(html_page2, "html.parser")

#soup = BeautifulSoup(html , "html.parser")
# use this (gets all <td> elements)
#cols = soup.findAll('td')
# or this (gets only <td> elements with class='h3')
#cols = soup.findAll('a', attrs={"class" : 'ko-option-template option'})
#print cols[0].renderContents() 
dataSite1 = {}
firstSiteEvent = [] #Array to store the event item  {first site}
firstSiteTime = [] #Array to store the event item data time {first site}
firstSiteOdds = [] #Array to store the event item odds {first site}

dataSite2 = {}
secSiteEvent = [] #Array to store the event item  {first site}
secSiteTime = [] #Array to store the event item data time {first site}
secSiteOdds = [] #Array to store the event item odds {first site}

r1 = re.compile('[A-z]') #search for the letters
r2 = re.compile('[0-9]') #search for the numbers  

# SITE 1 SCRAPING BEGIN ################################################
for eventName in soup1.find_all('a', attrs={"class" : 'eventNameLink'}):
    if (eventName.get_text()).find('@')==-1:
        firstSiteEvent.append(eventName.get_text())
        #print eventName.get_text()

for eventDate in soup1.find_all('span', attrs={"class" : 'StartTime'}):
    firstSiteTime.append(eventDate.get_text())
    #print eventDate.get_text()
    
for betsOffers in soup1.find_all('span', attrs={"class" : 'priceText wide  EU'}):  
    firstSiteOdds.append(betsOffers.get_text())
    #print betsOffers.get_text()
# SITE 1 SCRAPING END ##################################################    

# Normalize the data from site 1 #
junperT1 = 0 
junperDraw = 1
junperT2 = 2 
for el in range(len(firstSiteEvent)):
    if (el < len(firstSiteTime)):
        dataSite1[el] = {
           'teams': firstSiteEvent[el],
           'date': firstSiteTime[el],
           'firstTeamWin': firstSiteOdds[junperT1],
           'draw': firstSiteOdds[junperDraw],
           'secondTeamWin': firstSiteOdds[junperT2]
        }
        junperT1 += 3 
        junperDraw += 3
        junperT2 += 3
    
 
# SITE 2 SCRAPING BEGIN ################################################
for eventName in soup2.find_all('span', attrs={"class" : 'ng-binding ng-scope'}):
     if (eventName.get_text()).find('Draw')==-1:
         if (filter(r1.match, eventName.get_text())):
             secSiteEvent.append(eventName.get_text())
             #print eventName.get_text()
         if (filter(r2.match, eventName.get_text())):
             secSiteOdds.append(eventName.get_text())
             #print eventName.get_text()    

# SITE 2 SCRAPING END ##################################################    

# Normalize the data from site 2 #

jumperFT1 = 0
jumperFT2 = 1

junperT1 = 0 
junperDraw = 2
junperT2 = 1 
for el in range(len(secSiteEvent)):
    if (el < (len(secSiteOdds)/3)):
            dataSite2[el] = {
               'teams': secSiteEvent[jumperFT1] + " v " + secSiteEvent[jumperFT2],
               'firstTeamWin': secSiteOdds[junperT1],
               'draw': secSiteOdds[junperDraw],
               'secondTeamWin': secSiteOdds[junperT2]
            }
            jumperFT1 += 2
            jumperFT2 += 2
            junperT1 += 3 
            junperDraw += 3
            junperT2 += 3

#print dataSite1[0]['teams']
#print dataSite2[0]['teams']

################################################ Begin Compare to find the Arbitrage #########################################################

partOfAccountBalance = 0

for i in range(len(dataSite1)):
    for x in range(len(dataSite2)):
        if( dataSite1[i]['teams'] == dataSite2[x]['teams']):
           print dataSite1[i]['teams'] + " # " + dataSite2[x]['teams']
           print "odds team 1: " + dataSite1[i]['firstTeamWin'] + " odds team 2: " + dataSite2[x]['secondTeamWin']
           resT1 = (( 1/ float(dataSite1[i]['firstTeamWin']) ))*100
           resT2 = (( 1/ float(dataSite2[x]['secondTeamWin']) ))*100
           resT1Draw = (( 1/ float(dataSite1[i]['draw']) ))*100
           resT2Draw = (( 1/ float(dataSite2[x]['draw']) ))*100
           print resT1
           print resT2
           res = resT1+resT2
           resDraw = resT1Draw+resT2Draw
           print "arbitrage: " + str(res) + "%"
           # Calculate for the team win odds
           if ( res >= 95.00 and res < 99.00 ):
               truncResultFirst = (partOfAccountBalance * resT1) / res
               truncResultSec = (partOfAccountBalance * resT2) / res
           # Calculate for the teams draw
           if ( resDraw >= 95.00 and resDraw < 99.00 ):
               truncResultFirstDraw = (partOfAccountBalance * resT1Draw) / resDraw
               truncResultSecDraw = (partOfAccountBalance * resT2Draw) / resDraw    
               
# 95% 99%  

# (partOfAccountBalance * team1Percentage) / arbitrageResult = truncResultFirst
# (partOfAccountBalance * team2Percentage) / arbitrageResult = truncResultSec

# place the bet on first team on first site with the amount of truncResultFirst
# place the bet on sec team on sec site with the amount of truncResultSec
           


################################################ End Compare to find the Arbitrage #########################################################


# place the bet on site 1

# Browser 
br = mechanize.Browser() 

# Enable cookie support for urllib2 
cookiejar = cookielib.LWPCookieJar() 
br.set_cookiejar( cookiejar ) 

# Broser options 
br.set_handle_equiv( True ) 
br.set_handle_gzip( True ) 
br.set_handle_redirect( True ) 
br.set_handle_referer( True ) 
br.set_handle_robots( False ) 

# 
br.set_handle_refresh( mechanize._http.HTTPRefreshProcessor(), max_time = 1 ) 

br.addheaders = [ ( 'User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1' ) ] 

# authenticate 
br.open( "https://www.youwin.com" ) 
br.select_form( name="loginBox" ) 
# these two come from the code you posted
# where you would normally put in your username and password
br[ "username" ] = "MarkHalmond"
br[ "password" ] = "FGH44322"
res = br.submit() 

url = br.open( "https://www.youwin.com/t/customer/myaccount.aspx?" ) 
returnPage = url.read() 

#print "Success!\n"
print returnPage

#####################################################  Place the bet on site 2 #######################################
# Browser 
br1 = mechanize.Browser() 

# Enable cookie support for urllib2 
cookiejar = cookielib.LWPCookieJar() 
br1.set_cookiejar( cookiejar ) 

# Broser options 
br1.set_handle_equiv( True ) 
br1.set_handle_gzip( True ) 
br1.set_handle_redirect( True ) 
br1.set_handle_referer( True ) 
br1.set_handle_robots( False ) 

# ?? 
br1.set_handle_refresh( mechanize._http.HTTPRefreshProcessor(), max_time = 1 ) 

br1.addheaders = [ ( 'User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1' ) ] 

# authenticate 
br1.open( "https://www.pinnaclesports.com/en/login" ) 
br1.select_form( name="loginForm" ) 
# these two come from the code you posted
# where you would normally put in your username and password
br1[ "CustomerId" ] = "MT824253"
br1[ "Password" ] = "@WELCOME1"
res = br1.submit() 

url = br1.open( "https://www1.pinnaclesports.com/members/canvas.asp#/Members/TrTeaserSelect.asp" ) 
returnPage = url.read() 

#print "Success!\n"
print returnPage