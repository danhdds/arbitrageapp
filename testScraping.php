<?php   
/*
* @Author Danilo
* this script do the scarping given a url
* it extract the app's:
* Icon
* Name
* Dev
* Link
* Category
* Size
* Rating
* Main Image
* Video
*/


$bs_name = array();
$bs_postcode = array();
$bs_tel = array();
$bs_mobile = array();

$url = 'https://br.betboo.com/apostas-futebol/inglaterra-premier-league/1-102-386195.html'; // Url to parse

$output = file_get_contents($url);  // store url in a variable
//echo $output;	
$dom = new DOMDocument; // create dom object

$dom->loadHTML($output); // load the url content on dom object

$finder = new DomXPath($dom); // here we create the finder 

$appList = array(); // create the array to store the apps information

/* grab Business name */

$classname="markettypes"; // here we create the class that we want to grab its value

$nodes = $finder->query("//*[contains(@id, '$classname')]"); // find the class

foreach ($nodes as $node) {
   $name = $node->nodeValue;
   array_push($bs_name, $name);
   //echo  $name . '<br>';
} // end foreach


echo json_encode($bs_name);


?>