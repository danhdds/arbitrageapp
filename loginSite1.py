import cookielib 
import urllib2 
import mechanize 

# Browser 
br = mechanize.Browser() 

# Enable cookie support for urllib2 
cookiejar = cookielib.LWPCookieJar() 
br.set_cookiejar( cookiejar ) 

# Broser options 
br.set_handle_equiv( True ) 
br.set_handle_gzip( True ) 
br.set_handle_redirect( True ) 
br.set_handle_referer( True ) 
br.set_handle_robots( False ) 

# 
br.set_handle_refresh( mechanize._http.HTTPRefreshProcessor(), max_time = 1 ) 

br.addheaders = [ ( 'User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1' ) ] 

# authenticate 
br.open( "https://www.youwin.com" ) 
br.select_form( name="loginBox" ) 
# these two come from the code you posted
# where you would normally put in your username and password
br[ "username" ] = "MarkHalmond"
br[ "password" ] = "FGH44322"
res = br.submit() 

url = br.open( "https://www.youwin.com/t/customer/myaccount.aspx?" ) 
returnPage = url.read() 

#print "Success!\n"
print returnPage